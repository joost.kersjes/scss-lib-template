# Writing reusable Sass

Sass has multiple ways to package reusable styles:

- [`@use`](https://sass-lang.com/documentation/at-rules/use/)
- [`@forward`](https://sass-lang.com/documentation/at-rules/forward/)
- ~~`@import`~~ (deprecated), **never** write this
- [`@mixin` and `@include`](https://sass-lang.com/documentation/at-rules/mixin)
- [`@extend`](https://sass-lang.com/documentation/at-rules/extend)

We'll go over what scenarios suit each of these at-rules the best.

## `@use`

- Almost all Sass files should be [partials](https://sass-lang.com/documentation/at-rules/use/#partials) a.k.a. modules (prefixed with `_`)
- Only use the [`!default` flag](https://sass-lang.com/documentation/at-rules/use/#configuration) for module variables that should be configurable by users
- Avoid using the [`!global` flag](https://sass-lang.com/documentation/variables/#shadowing) to keep things simple

With `@use`, you can load other files (i.e. modules) to reuse its contents. This
allows the library to be split up into [partials](https://sass-lang.com/documentation/at-rules/use/#partials)
which can be easily reused.

To create a partial, simply prefix the filename with an underscore (`_`).

The only files that should not be prefixed are those meant to be compiled on
their own. In a library, there should not be more than a handful of these, if
any at all.

If a module has multiple exported members that all use a variable that users
might want to change, the `!default` flag should be used. Here is an example:

```scss
// _cta.scss
$border: 1px solid hsl(0, 0%, 0%) !default;

@mixin circle {
  border: $border;
  // ... circle styles
}

@mixin pill {
  border: $border;
  // ... pill styles
}
```

```scss
// _button.scss
@use "cta" with (
  $border: 2px solid hsl(0, 0%, 15%)
);
```

This type of [configuration](https://sass-lang.com/documentation/at-rules/use/#configuration)
should be preferred over giving every `@mixin` a `$border` argument so the
resulting style from all mixins are consistent.

Sass also has a [`!global` flag](https://sass-lang.com/documentation/variables/#shadowing)
which can be used to "set a global variable’s value from within a local scope
(such as in a mixin)". Because this is rarely used, the behavior can be
confusing. Instead, you should prefer [CSS custom properties](https://sass-lang.com/documentation/style-rules/declarations/#custom-properties)
(a.k.a. CSS variables) for this use case.

## `@forward`

- Uncommon, but might suit your module structure
- Always `hide` things that are not used by the forwarding file

While `@forward` isn't very common, it can be useful for certain projects. Here is an example:

```scss
// _shape.scss
$radius: 1rem;

@mixin border-radius {
  border-radius: $radius;
}
```

```scss
// _button.scss
@use "shape";
@forward "shape" hide $radius;
```

This structure allows the user to reuse the border radius from the `button`
module instead of needing to find the module that defined it. This can also
help to reduce the amount of `@use` statements that a user needs to write.

## `@mixin` and `@include`

- Preferred way of creating reusable rule sets
- Keep every `@mixin` small and simple, reuse them often
- Avoid static rules that users might want to override, use [optional arguments](https://sass-lang.com/documentation/at-rules/mixin/#optional-arguments)

> For more, see this guideline: https://sass-guidelin.es/#mixins

A `@mixin` is the most common way to define a reusable rule set with Sass.

Mixins are at their best when they can be easily reused. The key to this is
keeping them small and simple. Here is an example:

```scss
@mixin rtl {
  :where([dir="rtl"]) & {
    @content;
  }
}
```

If you want to create another mixin for `ltr`, the best option is to copy-paste
this mixin and change the `"rtl"` string. It might not be DRY, but it does make
them very portable, easy to understand and easy to use.

When a mixin includes a static value, e.g. `padding: 1.5rem;`, it is likely
that users will want to override this in certain situations. They can do this
by simply declaring a new value below the `@include` line, but that can become
problematic for users to maintain if the library is updated frequently.

It is usually preferred to add [optional arguments](https://sass-lang.com/documentation/at-rules/mixin/#optional-arguments)
to mixins that have such static values. Here is an example:

```scss
@mixin basic($padding: 1.5rem) {
  padding: $padding;
  background-color: white;
  border-style: none;
  border-radius: 1.5rem;
}
```

## `@extend`

- Prefer `@include` over `@extend` for simplicity
- Use `@extend` to maintain relationships between selectors
- Almost always use [placeholder selectors](https://sass-lang.com/documentation/at-rules/extend/#placeholder-selectors) (prefixed with `%`) to define rule sets

> For more, see this guideline: https://sass-guidelin.es/#extend

Like `@include` for mixins, `@extend` can also reuse styles, but with CSS
selectors. Due to the added complexity that selectors bring, you should **prefer
`@include` for simple cases**.

The primary use of `@extend` should be to maintain the relationships and/or
constraints within extended selectors between rule sets. Here is an example:

```scss
%button {
  display: inline-block;
  // … button styles

  // Relationship: a %button that is a child of a %modal
  %modal > & {
    display: block;
  }
}

.button {
  @extend %button;
}

.modal {
  @extend %modal;
}
```

Output:

```css
.button {
  display: inline-block;
}
.modal > .button {
  display: block;
}
```

Notice that both times `@extend` is used, it extends from a [placeholder selector](https://sass-lang.com/documentation/at-rules/extend/#placeholder-selectors)
which is indicated by the `%` character. This allows the class names to be
defined by the user.
