import { dirname, resolve } from "node:path";
import { fileURLToPath } from "node:url";

import * as sass from "sass";
import sassTrue from "sass-true";
import { describe, it } from "vitest";

const sassTestFiles = Object.keys(
  import.meta.glob("./**/*.test.scss", { eager: true }),
).map((relativePath) => {
  return resolve(dirname(fileURLToPath(import.meta.url)), relativePath);
});

sassTestFiles.forEach((file) => sassTrue.runSass({ describe, it, sass }, file));
