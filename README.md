# scss-lib-template

## Getting started

🏗️ UNDER CONSTRUCTION

## Code style

- All Sass code should follow these [Sass Guidelines](https://sass-guidelin.es/)
- Code formatting is done with [Prettier](https://prettier.io/)
